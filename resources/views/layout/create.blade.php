@extends('layouts.admin_layout')

@push('css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css"
          integrity="sha256-rByPlHULObEjJ6XQxW/flG2r+22R5dKiAoef+aXWfik=" crossorigin="anonymous" />
<link href="https://fonts.googleapis.com/css2?family=MuseoModerno&family=Oswald&family=Roboto&display=swap" rel="stylesheet">
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
    
    <style>
        .main {
            position: relative;
            width: 500px;
            height: 500px;
            /* background-image: url(""); */
            border:1px solid black;
            background-color:#ddd;
            margin: 0 0 0 auto;
        }
        .drag_t ,.drag-resize{
            position: absolute;
            cursor: all-scroll;
        }
        .controls {
            position: fixed;
            width: calc(100% - 1200px);
            height: 100vh;
        }
        #switch{
            height: 0;
            width: 0;
            visibility: hidden;
        }

        #switch +label {
            cursor: pointer;
            text-indent: -9999px;
            width: 70px;
            height: 20px;
            background: #555555;
            display: block;
            border-radius: 100px;
            position: relative;
        }

        #switch +label:after {
            content: '';
            position: absolute;
            top: -5px;
            left: 5px;
            width: 30px;
            height: 30px;
            background: #fff;
            border-radius: 25px;
            transition: 0.3s;
        }

        #switch:checked + label {
            background: #ffffff;
        }

        #switch:checked + label:after {
            left: calc(100% - 4px);
            transform: translateX(-100%);
            background: #555555;
        }

        #switch +label:active:after {
            width: 40px;
        }
    </style>

@endpush

@section('content')


<div class="logo-c">
        <form action="#" method="POST" id="form">
            @csrf
            <!-- <br> -->
            <!-- <br> -->
            <!-- <input type="checkbox" name="isLogo" checked onchange="toggleLogo()"> logo <br>
            <input type="number" name="logo_x" id="logo_x" value="0"> X
            <input type="number" name="logo_y" id="logo_y" value="0"> Y <br>
            <input type="number" name="logo_R" id="logo_ratio" value="1000" onchange="setRatio(this)"> w and H -->
{{--            <input type="number" id="logo_w" disabled value="0"> W--}}
{{--            <input type="number" id="logo_h" disabled value="0"> H--}}

            <!-- <br>
            <br>
            <br> -->
            <input type="checkbox" name="isText" checked onchange="toggletext()"> Display <br>
            <input type="number" id="text_x" name="text_x"  value="0"> X
            <input type="number" id="text_y" name="text_y"  value="0"> Y <br>
            <input type="number" id="text_size" name="font_size" onchange="setFontSize(this)" value="16"> size <br>
            <select name="text_font_family" id="text_font_family" onchange="setFontFamily(this)">
                <option value="MuseoModerno" selected>MuseoModerno</option>
                <option value="Oswald">Oswald</option>
                <option value="Roboto">Roboto</option>
            </select>  font family
            <br>

            <input type="color" id="text_color" >

            <input type="submit" value="submit">
        </form>



    </div>

<div class="main mr-5">
<span id="t_0" class="drag_t" onclick="activeText(this)"
          style="
              left: 10px;
              top: 10px;
              font-size: 20px;
              font-family: Oswald;
              color: #000000;
              display:'';"
    >admin@gmail.com</span>
    <span id="t_1" class="drag_t" onclick="activeText(this)"
          style="
              left: 10px;
              top: 50px;
              font-size: 20px;
              font-family: Oswald;
              color: #000000;
              display:'';"
    >+91 98989 98989</span>
    <span id="t_2" class="drag_t" onclick="activeText(this)"
          style="
              left: 10px;
              top: 90px;
              font-size: 20px;
              font-family: Oswald;
              color: #000000;
              display:'';"
    >@admin</span>

</div>

@endsection
@push('script')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js" integrity="sha256-KM512VNnjElC30ehFwehXjx1YCHPiQkOPmqnrWtpccM=" crossorigin="anonymous"></script> -->
<!-- <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script> -->

    <script >
        
        function activeText({id}){
            activeLogo_i=id.slice('_')[2];
            console.log(activeLogo_i);
        }
        let dataObj={
            imgs:[],
            texts:[],
        };
        let activeLogo_i=0;
        let activeText_i=0;
        let img = {
            _id:'',
            _x:'',
            _y:'',
            _ratio:'',
            _isDisplay:'',
            _width:'',
            _height:'',
            _logo_type:'',
        }
        let text = {
            _id:'',
            _x:'',
            _y:'',
            _isDisplay:'',
            _string:'',
            _font_size:'',
            _font_family:'',
            _font_color:'',
            _text_type:'',
        }
        $(document).ready(()=>{
            $('.drag_t').draggable({
                containment: "parent",
                stop: function( event, ui ) {
                    activeText(ui.helper[0]);
                    setTextData(ui.position);
                    // $('#d_text_x')[0].value=ui.position.left;
                    // $('#d_text_y')[0].value=ui.position.top;
                    // activeText(ui.helper[0]);
                    // changeText();
                }
            })
        });

        function setTextData(position) {
            console.log(position);
            $('#text_color').val();
        }
        
        // $('#t_0').draggable();

            // let active_logo_i='0';
            // let active_text_i='0';
            // let logoStart="#l_";
            // let textStart="#t_";
            // let darkMode=false;
            // // $(document).ready(()=>{
                // $(".drag-resize").draggable({
                //         stop: function( event, ui ) {
                //             $('#d_logo_x')[0].value=ui.position.left;
                //             $('#d_logo_y')[0].value=ui.position.top;
                //             activeLogo(ui.helper[0]);
                //             changeLogo();
                //         }
                //     }
                // );
                // $(".drag_t").draggable({
                //     containment: "parent",
                //     stop: function( event, ui ) {
                //         $('#d_text_x')[0].value=ui.position.left;
                //         $('#d_text_y')[0].value=ui.position.top;
                //         activeText(ui.helper[0]);
                //         changeText();
                //     }
                // })
            // });
            // function changeMode(){
            //     darkMode=!darkMode;
            //     $('.mode-e')[0].style.backgroundColor=darkMode?'#4C4B4B':'#ffffff';
            // }

            // function activeLogo({id}) {
            //     active_logo_i=id.split("_")[1];
            //     setLogoData();
            // }
            // function setLogoData() {
            //     const id =logoStart+active_logo_i;
            //     const selectedLogo=$(id)[0];

            //     $('#d_selected_logo')[0].innerHTML =$(id).eq(0).attr("alt");
            //     $('#d_logo_R')[0].value =selectedLogo.width;
            //     $('#d_logo_x')[0].value =$(id).eq(0).position().left;
            //     $('#d_logo_y')[0].value = $(id).eq(0).position().top;
            //     $('#d_l_isDisplay')[0].checked=(selectedLogo.style.display==="none")?false:true;
            // }
            // function changeLogo() {
            //     const _r = $('#d_logo_R')[0].value;
            //     const isDisplay = $('#d_l_isDisplay').is(":checked");
            //     const _x=$('#d_logo_x')[0].value;
            //     const _y=$('#d_logo_y')[0].value;
            //     const id= logoStart + active_logo_i;

            //     const selectedlogo=$(id)[0];
            //     selectedlogo.style.width=_r+"px";
            //     selectedlogo.style.height=_r+"px";

            //     $('#l_isDisplay_'+active_logo_i)[0].value=isDisplay;
            //     $('#l_position_x_'+active_logo_i)[0].value=_x;
            //     $('#l_position_y_'+active_logo_i)[0].value=_y;
            //     $('#l_ratio_'+active_logo_i)[0].value=_r;
            //     setDisplayOfLogo(isDisplay);
            // }
            // function setDisplayOfLogo(isDisplay){
            //     const id= logoStart + active_logo_i;
            //     $(id)[0].style.display=isDisplay?"":"none";
            // }

            // function activeText({id}) {
            //     active_text_i=id.split("_")[1];
            //     setTextData();
            // }
            // function setTextData() {
            //     const id =textStart+active_text_i;
            //     const selectedText=$(id)[0];

            //     // console.log($(id).eq(0).position());
            //     $('#d_size')[0].value =selectedText.style.fontSize.split("px")[0];
            //     $('#d_family')[0].value=selectedText.style.fontFamily ;
            //     $('#d_color')[0].value= getHex(selectedText.style.color);
            //     $('#d_string')[0].innerHTML =selectedText.innerHTML;
            //     $('#d_text_x')[0].value =$(id).eq(0).position().left;
            //     $('#d_text_y')[0].value = $(id).eq(0).position().top;
            //     $('#d_isDisplay')[0].checked=(selectedText.style.display==="none")?false:true;
            // }
            // function changeText() {
            //     const fontFamily = $('#d_family')[0].value;
            //     const fontSize = $('#d_size')[0].value;
            //     const fontColor = $('#d_color')[0].value;
            //     const isDisplay = $('#d_isDisplay').is(":checked");
            //     const _x=$('#d_text_x')[0].value;
            //     const _y=$('#d_text_y')[0].value;
            //     // console.log(fontSize,fontColor,fontFamily,_x,_y,isDisplay);
            //     const id= textStart + active_text_i;
            //     const selectedText=$(id)[0];
            //     selectedText.style.fontSize=fontSize+"px";
            //     selectedText.style.fontFamily=fontFamily;
            //     selectedText.style.color=fontColor;

            //     $('#t_isDisplay_'+active_text_i)[0].value=isDisplay;
            //     $('#t_position_x_'+active_text_i)[0].value=_x;
            //     $('#t_position_y_'+active_text_i)[0].value=_y;
            //     $('#t_fontSize_'+active_text_i)[0].value=fontSize;
            //     $('#t_fontFamily_'+active_text_i)[0].value=fontFamily;
            //     $('#t_fontColor_'+active_text_i)[0].value =fontColor;
            //     setDisplayOfText(isDisplay);
            // }
            // function setDisplayOfText(isDisplay){
            //     const id= textStart + active_text_i;
            //     $(id)[0].style.display=isDisplay?"":"none";
            // }

            const hexDigits = new Array("0","1","2","3","4","5","6","7","8","9","a","b","c","d","e","f");
            function getHex(rgb) {
                //Function to convert rgb color to hex format
                rgb = rgb.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
                return "#" + hex(rgb[1]) + hex(rgb[2]) + hex(rgb[3]);
            }
            function hex(x) {
                return isNaN(x) ? "00" : hexDigits[(x - x % 16) / 16] + hexDigits[x % 16];
            }
    </script>
@endpush